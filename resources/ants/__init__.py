import numpy as np

from dataclasses import dataclass, field

import logging

logger = logging.getLogger('ants')

MAX_CITIES=30
MAX_DISTANCE=100

MAX_TOUR=MAX_CITIES*MAX_DISTANCE

MAX_ANTS=30


@dataclass
class Ant:
    current_city: int
    next_city: int
    path: list
    tour_length: float

MAX_TOUR = 30

MAX_TIME = MAX_TOUR * MAX_CITIES

INIT_PHEROMONE = 1.0 / MAX_CITIES

cities = None
distances = None
pheromones = None

ants = []

best = float('inf')
best_path = None

@dataclass
class ACOParameters:
    alpha: float
    beta: float
    rho: float
    qval: float


parameters = ACOParameters(alpha=1.0, beta = 5.0, rho = 0.5, qval = 100)

from scipy.spatial import distance

def initialize_cities():
    global cities, distances, pheromones
    cities = np.random.randint(low=0, high=MAX_DISTANCE, size=(MAX_CITIES,2))
    distances = distance.cdist(cities, cities, 'euclidean')
    pheromones = np.full([MAX_CITIES,MAX_CITIES], fill_value=INIT_PHEROMONE)


def initialize_colony():
    global ants
    for _ in range(MAX_ANTS):
        current_city= np.random.choice(MAX_CITIES)
        next_city=None
        path = [current_city]
        tour_length = 0.0
        ant = Ant(current_city,
                  next_city,
                  path,
                  tour_length)
        ants.append(ant)


def reset_colony():
    global best, best_path, ants
    print("Reset the colony")
    for ant in ants:
        #print(f"Best: {best}, best_path: {None if not best_path else best_path.tour_length}, current_ant: {ant.tour_length}")
        if ant.tour_length < best:
            print("New best!")
            best = ant.tour_length
            best_path = ant
            #print(f"Best: {best}, best_path: {None if not best_path else best_path.tour_length}, current_ant: {ant.tour_length}")

    initialize_colony()

def ant_product(_from,to):
    return pheromones[_from][to]**parameters.alpha * (1.0/distances[_from][to])**parameters.beta

def select_next_city(ant):
    _from = ant.current_city
    next_city = None
    denominator = 0.0

    not_visited = set(range(MAX_CITIES)).difference(set(ant.path))

    # Calculate the denominator using all the not visited cities
    for to in not_visited:
        denominator += ant_product(_from, to)

    threshold = np.random.rand()
    # Calculate the probability of transition for each not visited city
    for to in not_visited:
        p = ant_product(_from, to)/denominator
        threshold -= p
        if threshold <= 0:
            next_city = to
            break

    return next_city

def simulate_ants():
    global ants

    moving = 0

    for ant in ants:
        # Ant still has cities to visit
        while len(ant.path) < MAX_CITIES:
            ant.next_city = select_next_city(ant)
            ant.path.append(ant.next_city)
            ant.tour_length += distances[ant.current_city][ant.next_city]

            # Handle the final case (last city to last)
            if len(ant.path) == MAX_CITIES:
                ant.tour_length += distances[ant.path[-1]][ant.path[0]]

            ant.current_city = ant.next_city

        moving += 1

    return moving

from itertools import tee

def pairwise(iterable):
    "s -> (s0,s1), (s1,s2), (s2, s3), ..."
    a, b = tee(iterable)
    next(b, None)
    return zip(a, b)


def update_trails():
    global pheromones, ants

    # Pheromone evaporation
    pheromones *= (1.0 * parameters.rho)
    pheromones[pheromones < 0.0] = INIT_PHEROMONE

    # Add new pheromone to the trails
    for ant in ants:
        for (_from, to) in pairwise(ant.path):
            pheromones[_from][to] += (parameters.qval / ant.tour_length)
            pheromones[to][_from] = pheromones[_from][to]

    pheromones *= parameters.rho

def main():
    current_time = 0

    initialize_cities()
    initialize_colony()

    while(current_time < MAX_TIME):
        if simulate_ants() == 0:
            update_trails()
            #print(f"Best: {best}, best_path: {None if not best_path else best_path.tour_length}")
        if current_time != MAX_TIME:
            reset_colony()

            print(f"Time is {current_time}: {best}")

        current_time += 1

    print(f"Best tour: {best}")

import matplotlib.pyplot as plt

def plot_cities():
    #    plt.scatter(cities[:,0], cities[:,1])
    plt.plot(cities[:,0], cities[:,1], 'co')
    plt.xlim(0, MAX_DISTANCE)
    plt.ylim(0, MAX_DISTANCE)
    plt.show()


def plot_solution(path):
    plt.plot(cities[:,0], cities[:,1], 'co')
    plt.xlim(0, MAX_DISTANCE)
    plt.ylim(0, MAX_DISTANCE)

    for (_from, to) in pairwise(path):
        plt.arrow(cities[_from][0], cities[_from][1],
                  cities[to][0]- cities[_from][0], cities[to][1] - cities[_from][1],
                  color='b', length_includes_head=True)


    # Close the loop
    last = path[-1]
    first = path[0]
    plt.arrow(cities[last][0], cities[last][1],
                  cities[first][0]- cities[last][0], cities[first][1] - cities[last][1],
                  color='b', length_includes_head=True)

    plt.show()

import copy

def greedy():
    start = np.random.choice(MAX_CITIES)
    current = start
    path = [start]
    cost = 0.0

    local_distances = copy.deepcopy(distances)

    while len(path) < MAX_CITIES:
        distances_to = np.copy(local_distances[current,:])
        distances_to[path] = np.infty

        current = np.argmin(distances_to)
        path.append(current)
        cost += np.min(distances_to)
        print(path, cost, current)

    return cost, path
